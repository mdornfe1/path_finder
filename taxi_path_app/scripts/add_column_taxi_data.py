import pandas as pd
import pandas.io.sql as sqlio
import sqlalchemy as sa

TAXI_CONN = sa.create_engine('postgresql://postgres:Ieatqas7535#@localhost:5432/nyc_taxi_data')
MAP_CONN = sa.create_engine('postgresql://postgres:Ieatqas7535#@localhost:5432/nyc_routing')

sql = "SELECT * FROM green_trip_data;"
gd = sqlio.read_sql(sql, TAXI_CONN)
hours = pd.Series([dt.hour for dt in gd.lpep_pickup_datetime])
dows = pd.Series([dt.today().weekday() for dt in gd.lpep_pickup_datetime])
gd['pickup_hour'] = hours
gd['day_of_week'] = dows
gd.to_sql('green_trip_data', MAP_CONN, chunksize=50000)

sql = "SELECT * FROM yellow_trip_data;"
yd = sqlio.read_sql(sql, TAXI_CONN)
hours = pd.Series([dt.hour for dt in yd.tpep_pickup_datetime])
dows = pd.Series([dt.today().weekday() for dt in yd.tpep_pickup_datetime])
yd['pickup_hour'] = hours
yd['day_of_week'] = dows
yd.to_sql('yellow_trip_data', MAP_CONN, chunksize=25000)

